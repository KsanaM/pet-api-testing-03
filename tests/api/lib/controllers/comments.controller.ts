import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class CommentsController {
    async createComment(commentData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Comments`)
            .body(commentData)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
