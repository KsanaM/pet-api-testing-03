//Get all posts list : GET /api/Posts

import { expect } from "chai";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { RegisterController } from "../lib/controllers/register.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import userData from "./data/userData";

export let registerUserId: number;
export let registerUserEmail: string;
//export let registerUserEmail: string;

const register = new RegisterController();
const posts = new PostsController();
const schemas = require("./data/schemas_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));

describe(`Get all posts list and create new user`, () => {
    it(`should return 200 status code and all users when getting the user collection`, async () => {
        let response = await posts.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1);
    });
    it(`Get specific post by ID, status should be 200`, async () => {
        let response = await posts.getAllPosts();

        const targetId = 2878;

        const specificPost = response.body.find((post) => post.id === targetId);

        if (specificPost) {
            console.log("Specific Post:");
            console.log(specificPost);
        } else {
            console.log(`Post with ID ${targetId} not found.`);
        }
    });

    after(`Send the user credentials and register`, async () => {
        let response = await register.registerUser(userData);

        console.log(response.body);

        checkStatusCode(response, 201);
        checkResponseTime(response, 1000);
        //expect(response.body).to.be.jsonSchema(schemas.schema_register_auth);
    });
});
