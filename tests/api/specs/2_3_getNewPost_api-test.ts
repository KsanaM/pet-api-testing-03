//Get details of new post : GET /api/Posts

import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { newPostId, newPostAuthorId } from "./2_2_createPost_api-test";

const posts = new PostsController();

describe(`Get all posts list`, () => {
    it(`Get new post by ID, status should be 200`, async () => {
        let response = await posts.getAllPosts();

        console.log(newPostAuthorId, newPostId);

        let newPost = response.body.find((post) => post.id === newPostId);

        console.log("New post's", newPostId, "detailes was created by user", newPostAuthorId);
        console.log(newPost);
        //expect(response.body.author.id).to.be.equal(newPostAuthorId, "AuthorId does't match");

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });
});
