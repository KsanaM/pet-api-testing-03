// Create new comment POST /api/Comments

import { expect } from "chai";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";
import { CommentsController } from "../lib/controllers/comments.controller";
import userData from "./data/userData";
import { newPostId, newPostAuthorId } from "./2_2_createPost_api-test";

export let newCommentId, newCommentAuthorId;

const auth = new AuthController();
const comments = new CommentsController();
const schemas = require("./data/schemas_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));
//const { expect } = require('chai');

describe("Creating a new comment", () => {
    let email: string;
    let password: string;
    password = userData.password;
    email = userData.email;

    let accessToken: string;
    let userId: number;

    before(`Login and get the token`, async () => {
        let response = await auth.login(email, password);

        accessToken = response.body.token.accessToken.token;
        userId = response.body.user.id;

        //console.log(accessToken);
        console.log("User id is:", userId, "authorized");

        expect(response.body).to.be.jsonSchema(schemas.schema_register_auth);
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });

    it(`Send the own post data and create comment`, async () => {
        let commentData;
        commentData = {
            authorId: newPostAuthorId,
            postId: newPostId,
            body: "Great post!",
        };
        let commentBody;
        commentBody = commentData.body;

        let response = await comments.createComment(commentData, accessToken);

        newCommentId = response.body.id;
        newCommentAuthorId = response.body.author?.id;
        console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        //expect(response.body).to.be.jsonSchema(schemas.schema_comment);

        //expect(response.body.comment.body, "Text of new comment is as expected").to.be.equal(commentBody);
    });

    it(`Send another user's post data and create comment`, async () => {
        let anotherUserCommentData;
        anotherUserCommentData = {
            authorId: 4382,
            postId: 3063,
            body: "Great post!",
        };
        //let commentBody;
        //   commentBody = commentData.body;

        let response = await comments.createComment(anotherUserCommentData, accessToken);

        console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        //expect(response.body).to.be.jsonSchema(schemas.schema_comment);

        //expect(response.body.comment.body, "Text of new comment is as expected").to.be.equal(commentBody);
    });

    it(`Send comment when post isn't exist, should return 500`, async () => {
        let invalidCommentData;
        invalidCommentData = {
            authorId: 4382,
            postId: 1892837,
            body: "Great post!",
        };
        //let commentBody;
        //    commentBody = commentData.body;

        let response = await comments.createComment(invalidCommentData, accessToken);

        console.log(response.body);

        checkStatusCode(response, 500);
        checkResponseTime(response, 1000);
    });
});
