import { checkResponseTime, checkStatusCode } from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";
const auth = new AuthController();

describe("Use test data set for login", () => {
    let invalidCredentialsDataSet = [
        { email: "mailKs1@gmail.com", password: "    " },
        { email: "mailKs1@gmail.com", password: "" },
        { email: "mailKs1@gmail.com", password: "777777777" },
        { email: "mailKs1@gmail.com", password: "mailKs1@gmail.com" },
        { email: "mailKs1@gmail.com", password: "kitty777" },
        { email: "mailKs1@gmail.com", password: "Kitty77" },
        { email: "mailKs1@gmail.com", password: "kitti77" },
        { email: "mailKs1@gmail.com", password: "kitti" },
    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`should not login using invalid credentials : '${credentials.email}' + '${credentials.password}'`, async () => {
            let response = await auth.login(credentials.email, credentials.password);

            checkStatusCode(response, 401);
            checkResponseTime(response, 3000);
        });
    });
});
