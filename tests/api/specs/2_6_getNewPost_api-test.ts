//Get new post detailes : GET /api/Posts

import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { newPostId, newPostAuthorId } from "./2_2_createPost_api-test";
import { newCommentId, newCommentAuthorId } from "./2_4_createComment_api-test";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import userData from "./data/userData";

const users = new UsersController();
const auth = new AuthController();
const posts = new PostsController();

describe(`Get all posts list`, () => {
    it(`Get new post by ID, status should be 200`, async () => {
        let response = await posts.getAllPosts();

        console.log("new post id/author", newPostId, newPostAuthorId);
        console.log("new comment id/author", newCommentId, newCommentAuthorId);

        let newPost = response.body.find((post) => post.id === newPostId);

        console.log("New post's", newPostId, "detailes was created by user", newPostAuthorId);
        console.log(newPost);
        //expect(response.body.author.id).to.be.equal(newPostAuthorId, "AuthorId does't match");

        // add test to check amaunt of likes before and after put like

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });

    let email: string;
    let password: string;
    password = userData.password;
    email = userData.email;

    let accessToken: string;
    let id: number;

    it(`Login and get the token`, async () => {
        let response = await auth.login(email, password);

        accessToken = response.body.token.accessToken.token;
        id = response.body.user.id;

        //console.log(accessToken);
        console.log("User id is:", id, "authorized");

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });

    after(`should delete user from the user collection`, async () => {
        let response = await users.deleteUser(id, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });
});
