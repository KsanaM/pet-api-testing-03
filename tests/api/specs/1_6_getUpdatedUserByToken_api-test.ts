///Get details of the current logged in user: GET /api/Users/fromToken

import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import userData from "./data/userData";
import { checkStatusCode, checkResponseTime, checkJsonSchema } from "../../helpers/functionsForChecking.helper";
import { updatedUserName } from "./1_5_updateUser_api-test";

const users = new UsersController();
const schemas = require("./data/schemas_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));

const auth = new AuthController();

describe("Get details of the current updated user", () => {
    let accessToken: string;
    let email: string;
    let password: string;
    password = userData.password;
    email = userData.email;
    let schema;

    before(`Login and get the token`, async () => {
        schema = schemas.schema_register_auth;

        let response = await auth.login(email, password);

        accessToken = response.body.token.accessToken.token;

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonSchema(response.body, schema);
    });
    it(`Return correct details current updated user`, async () => {
        schema = schemas.schema_currentUser;

        let response = await users.getCurrentUser(accessToken);

        expect(response.body.userName).to.be.equal(updatedUserName, "User name after update isn't correct");

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonSchema(response.body, schema);
    });
});
