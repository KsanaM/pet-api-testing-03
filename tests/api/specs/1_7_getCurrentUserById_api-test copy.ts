//Get details of current user by its id: GET /api/Users/{id}
import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import userData from "./data/userData";
import { checkStatusCode, checkResponseTime, checkJsonSchema } from "../../helpers/functionsForChecking.helper";
//import {currentUpdatedUserId} from "./1_6_getUpdatedUserDetails_api-test"

const users = new UsersController();
const schemas = require("./data/schemas_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));
const auth = new AuthController();

describe("Get details of the current updated user", () => {
    let accessToken: string;
    let email: string;
    let password: string;
    password = userData.password;
    email = userData.email;
    let currentUserId: number;
    let schema;

    before(`Login and get the token`, async () => {
        let response = await auth.login(email, password);

        accessToken = response.body.token.accessToken.token;
        schema = schemas.schema_register_auth;

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonSchema(response.body, schema);
    });

    schema = schemas.schema_currentUser;

    it(`Return correct details current updated user`, async () => {
        let response = await users.getCurrentUser(accessToken);

        currentUserId = response.body.id;

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonSchema(response.body, schema);
    });

    it("Should return details of current user by its id", async () => {
        let response = await users.getUserById(currentUserId);

        // add test to compare body current user ane user by id

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonSchema(response.body, schema);
    });
});
