//Get all user list : GET /api/Users

import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime, checkJsonSchema } from "../../helpers/functionsForChecking.helper";
import { registerUserId } from "./1_1_registerUser_api-test";

const users = new UsersController();
const schemas = require("./data/schemas_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));
let schema;

describe(`Get all user list`, () => {
    it(`should return 200 status code and all users when getting the user collection`, async () => {
        let response = await users.getAllUsers();

        //schema = schemas.schema_allUsers;
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        //checkJsonSchema(response.body, schema);
    });

    it("Should return the registered user details", async () => {
        let response = await users.getUserById(registerUserId);

        schema = schemas.schema_currentUser;

        console.log("Registered user details:", response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonSchema(response.body, schema);
    });

    it(`Should return 404 error when getting user details with invalid id`, async () => {
        let invalidUserId = 673892;

        let response = await users.getUserById(invalidUserId);
        expect(response.statusCode, `Status Code should be 404`).to.be.equal(404);

        checkStatusCode(response, 404);
        checkResponseTime(response, 1000);
    });

    it(`should return 400 error when getting user details with invalid id type`, async () => {
        let invalidUserId = "MyID - 38";

        let response = await users.getUserById(invalidUserId);
        checkStatusCode(response, 400);
        checkResponseTime(response, 1000);
    });
});
