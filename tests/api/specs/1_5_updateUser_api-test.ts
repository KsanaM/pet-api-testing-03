//Update current user: PUT /api/Users
import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import userData from "./data/userData";
import { checkStatusCode, checkResponseTime, checkJsonSchema } from "../../helpers/functionsForChecking.helper";
export let updatedUserName: string;

const users = new UsersController();
const schemas = require("./data/schemas_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));
let schema;

const auth = new AuthController();

describe("Update user profile (change userName)", () => {
    let accessToken: string;

    let email: string;
    let password: string;
    password = userData.password;
    email = userData.email;

    let oldUserData, newUserData;

    before(`Login and get the token`, async () => {
        let response = await auth.login(email, password);

        accessToken = response.body.token.accessToken.token;
        schema = schemas.schema_register_auth;

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonSchema(response.body, schema);
    });

    it(`should return correct details of the current user`, async () => {
        let response = await users.getCurrentUser(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        oldUserData = response.body;
    });

    schema = schemas.schema_currentUser;

    it(`should update username using valid data`, async () => {
        function replaceLastThreeWithRandom(str: string): string {
            const randomStr = Math.random().toString(36).substring(2, 5);
            return str.slice(0, -3) + randomStr;
        }
        newUserData = {
            id: oldUserData.id,
            avatar: oldUserData.avatar,
            email: oldUserData.email,
            userName: replaceLastThreeWithRandom(oldUserData.userName),
        };

        let response = await users.updateUser(newUserData, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response, 1000);
        checkJsonSchema(response.body, schema);
    });

    it(`should return correct user details by id after updating`, async () => {
        let response = await users.getUserById(oldUserData.id);

        updatedUserName = response.body.userName;

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonSchema(response.body, schema);

        expect(response.body).to.be.deep.equal(newUserData, "User details isn't correct");
    });
});
