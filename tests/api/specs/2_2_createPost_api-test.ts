//Create new post POST /api/Posts

import { expect } from "chai";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import userData from "./data/userData";

export let newPostId, newPostAuthorId;

const auth = new AuthController();
const posts = new PostsController();
const schemas = require("./data/schemas_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));
//const { expect } = require('chai');

describe("Creating a new post", () => {
    let email: string;
    let password: string;
    password = userData.password;
    email = userData.email;

    let accessToken: string;
    let userId: number;

    before(`Creating post without authorization, wrong accessToken`, async () => {
        let postData: object = {
            authorId: userId,
            previewImage: "string",
            body: "My first post, please like",
        };
        accessToken = "myToken";
        let response = await posts.createPost(postData, accessToken);

        checkStatusCode(response, 401);
        checkResponseTime(response, 1000);
    });
    before(`Login and get the token`, async () => {
        let response = await auth.login(email, password);

        accessToken = response.body.token.accessToken.token;
        userId = response.body.user.id;

        //console.log(accessToken);
        console.log("User id is:", userId, "authorized");

        expect(response.body).to.be.jsonSchema(schemas.schema_register_auth);
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });

    it(`Send the post data and create post`, async () => {
        let postData;
        postData = {
            authorId: userId,
            previewImage: "string",
            body: "My next post, please like",
        };
        let postBody;
        postBody = postData.body;

        let response = await posts.createPost(postData, accessToken);

        newPostId = response.body.id;
        newPostAuthorId = response.body.author?.id;
        console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        //expect(response.body).to.be.jsonSchema(schemas.schema_Post);

        //expect(response.body.post.body, "Text of new post is as expected").to.be.equal(postBody);
    });
});
