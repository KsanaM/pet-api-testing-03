//Register a new user: POST /api/Register

import { checkStatusCode, checkResponseTime, checkJsonSchema } from "../../helpers/functionsForChecking.helper";
import { RegisterController } from "../lib/controllers/register.controller";
import userData from "./data/userData";

export let registerUserId: number;
export let registerUserEmail: string;
//export let registerUserEmail: string;

const register = new RegisterController();
const schemas = require("./data/schemas_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));

let schema = schemas.schema_register_auth;

describe("Register a new user:", () => {
    it(`Send the user credentials and register`, async () => {
        let response = await register.registerUser(userData);

        registerUserId = response.body.user.id;

        console.log(response.body);

        checkStatusCode(response, 201);
        checkResponseTime(response, 1000);
        //checkJsonSchema(response.body, schema);
    });
});
