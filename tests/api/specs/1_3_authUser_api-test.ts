//Log in using valid credentials from the first step: POST /api/Auth/login

//import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import userData from "./data/userData";
export let authUserId, authUserName;
//import {registerUserId} from "./1_1_registerUser_api-test";

import { checkStatusCode, checkResponseTime, checkJsonSchema } from "../../helpers/functionsForChecking.helper";

const schemas = require("./data/schemas_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));
const auth = new AuthController();
const { expect } = require("chai");
const schema = schemas.schema_register_auth;

describe("Login newUser and token usage", () => {
    let email: string;
    let password: string;
    password = userData.password;
    email = userData.email;

    let accessToken: string;
    let userId: number;

    it(`Login and get the token`, async () => {
        let response = await auth.login(email, password);

        accessToken = response.body.token.accessToken.token;
        userId = response.body.user.id;
        console.log(accessToken);
        console.log("User id is:", userId, "authorized");

        authUserId = response.body.user.id;
        authUserName = response.body.user.userName;

        //expect('Registered', registerUserId,`should be equal auth userId`, userId).to.be.equal(userId);
        //(tryed to add test that userId equal registered user id)

        checkJsonSchema(response.body, schema);
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });
});
