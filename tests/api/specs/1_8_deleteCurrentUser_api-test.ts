//Delete current user: DELETE /api/Users/{id}
//import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import userData from "./data/userData";
import { checkStatusCode, checkResponseTime, checkJsonSchema } from "../../helpers/functionsForChecking.helper";

const auth = new AuthController();
const users = new UsersController();
const chai = require("chai");
const schemas = require("./data/schemas_testData.json");
const { expect } = require("chai");

describe("Delete current user and ensure it was deleted", () => {
    let email: string;
    let password: string;
    let userIdForDelete: number;
    let authUserName: string;
    password = userData.password;
    email = userData.email;
    const schema = schemas.schema_register_auth;

    before(`Login and get the token before delete`, async () => {
        let response = await auth.login(email, password);

        accessToken = response.body.token.accessToken.token;

        authUserName = response.body.user.userName;
        console.log("user data before delete", response.body);

        id = response.body.user.id;
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonSchema(response.body, schema);
    });

    let accessToken: string;
    let id: number;
    let currentUserId: number;
    let currentUserName: string;

    it(`Return correct details of the current user`, async () => {
        let response = await users.getCurrentUser(accessToken);

        currentUserId = response.body.id;
        currentUserName = response.body.userName;
        //console.log("Current user details:", currentUserData);

        expect(currentUserName).to.be.equal(authUserName, "UserName does't match");

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonSchema(response.body, schema);
    });

    it(`should delete user from the user collection`, async () => {
        id = currentUserId;

        let response = await users.deleteUser(id, accessToken);
        //console.log("id", userIdForDelete)
        checkStatusCode(response, 204);
        checkResponseTime(response, 1000);
    });

    it(`should return 401 when try to delete deleted user again`, async () => {
        id = currentUserId;
        let response = await users.deleteUser(id, accessToken);

        checkStatusCode(response, 401);
        checkResponseTime(response, 1000);
    });

    after("Get deleted user, should return 404", async () => {
        let response = await users.getUserById(userIdForDelete);

        checkStatusCode(response, 404);
        checkResponseTime(response, 1000);
        checkJsonSchema(response.body, schema);
    });
});
