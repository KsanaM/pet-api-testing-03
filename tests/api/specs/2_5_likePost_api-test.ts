// Like the post POST /api/Posts/like

import { expect } from "chai";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import userData from "./data/userData";
import { newPostId, newPostAuthorId } from "./2_2_createPost_api-test";

const auth = new AuthController();
const posts = new PostsController();
const schemas = require("./data/schemas_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));
//const { expect } = require('chai');

describe("Creating a new post", () => {
    let email: string;
    let password: string;
    password = userData.password;
    email = userData.email;

    let accessToken: string;
    let userId: number;

    before(`Login and get the token`, async () => {
        let response = await auth.login(email, password);

        accessToken = response.body.token.accessToken.token;
        userId = response.body.user.id;

        //console.log(accessToken);
        console.log("User id is:", userId, "authorized");

        expect(response.body).to.be.jsonSchema(schemas.schema_register_auth);
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });

    it(`Send the post data and like post`, async () => {
        let likeData;
        likeData = {
            entityId: newPostId,
            isLike: true,
            userId: userId,
        };
        let likeBody;
        likeBody = likeData.body;

        let response = await posts.createPost(likeData, accessToken);

        console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });
    // Add after  - get post and save amount of likes
});
