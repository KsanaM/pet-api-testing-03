//Get details of the current logged in user: GET /api/Users/fromToken

import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import userData from "./data/userData";
import { authUserName } from "./1_3_authUser_api-test";
import { checkStatusCode, checkResponseTime, checkJsonSchema } from "../../helpers/functionsForChecking.helper";

const auth = new AuthController();
const users = new UsersController();
const schemas = require("./data/schemas_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));
let schema;

describe("Get details of the current logged in user", () => {
    let accessToken: string;
    let email: string;
    let password: string;

    password = userData.password;
    email = userData.email;

    let currentUserData: object;

    before(`Login and get the token`, async () => {
        let response = await auth.login(email, password);

        accessToken = response.body.token.accessToken.token;
        schema = schemas.schema_register_auth;

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonSchema(response.body, schema);
    });
    it(`Return correct details of the current user`, async () => {
        let response = await users.getCurrentUser(accessToken);

        let currentUserId: number;
        let currentUserName: string;

        schema = schemas.schema_currentUser;

        currentUserId = response.body.id;
        currentUserName = response.body.userName;
        console.log("Current user details:", currentUserData);

        expect(currentUserName).to.be.equal(authUserName, "UserName does't match");

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonSchema(response.body, schema);
    });
});
