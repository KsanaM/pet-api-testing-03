//Template for tests with authorization

import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";

const users = new UsersController();
const schemas = require("./data/schemas_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));

const auth = new AuthController();

xdescribe("Get details of the current logged in use", () => {
    let accessToken: string;
    let email: string;
    let password: string = "kittY77";
    let userId: number;

    before(`Login and get the token`, async () => {
        let response = await auth.login(email, password);

        accessToken = response.body.token.accessToken.token;
        userId = response.body.user.id;

        console.log("User id is:", userId, "authorized");

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    });
});
